import React, { Component } from 'react';
import './App.css';
import Home from './components/home/home';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import NewsFeed from "./components/newfeed/newfeed";
class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <div className="container">
            <Route exact path="/" component={Home} />
          </div>
          <Route path="/news" component={NewsFeed} />
        </div>
      </Router>
    );
  }
}

export default App;
