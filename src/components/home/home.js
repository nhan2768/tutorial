import React, { Component } from 'react';
import ColorPicker from "../home/colorpicker";
import SizeSetting from "../home/sizesetting";
import Result from "../home/result"
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      color: 'red',
      fontSize: 15
    };
    this.onSetColor = this.onSetColor.bind(this);
    this.onChangeSizeDown = this.onChangeSizeDown.bind(this);
    this.onChangeSizeUp = this.onChangeSizeUp.bind(this);
  }
  onSetColor(params) {
    this.setState({
      color: params
    })
  }
  onChangeSizeDown() {
    this.setState({
      fontSize: this.state.fontSize - 2
    });
  }
  onChangeSizeUp() {
    this.setState({
      fontSize: this.state.fontSize + 2
    });
  }
  
  render() {
    return (
      <div>
        <div className="row">
          <ColorPicker color={this.state.color} onReceiveColor={this.onSetColor} />
          <SizeSetting fontSize={this.state.fontSize} onChangeSizeDown={this.onChangeSizeDown }  onChangeSizeUp={this.onChangeSizeUp}/>
        </div>
        <hr></hr>
        <Result color={this.state.color} fontSize={this.state.fontSize}
        />
      </div>
    );
  }
}

export default Home;
