import React, { Component } from 'react';

class ColorPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            color: ['red', 'green', 'blue', '#ccc']
        }
    }
    showColor(color){
        return {
            backgroundColor: color
        }
    }
    setActionColor(color){
        this.props.onReceiveColor(color);
    }
    render() {
        var elmColors = this.state.color.map((color, index) => {
            return <span 
            key={ index }
            style={ this.showColor(color) }
            className={ this.props.color === color ? 'active' : '' }
            onClick={ () => this.setActionColor(color) }
            ></span>
        });
        return (
            <div className="col">
                <div className="card">
                    <div className="card-header">Color Picker</div>
                    <div className="card-body">
                        {elmColors}
                    </div>
                </div>
            </div>
        );
    }
}

export default ColorPicker;
