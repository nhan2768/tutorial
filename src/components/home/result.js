import React, { Component } from 'react';

class Result extends Component {
    setStyle() {
        return{
            color: this.props.color,
            fontSize: this.props.fontSize
        };
    }
    render() {
        return (
            <div className="card" >
                <div className="card-header">Size {this.props.fontSize}</div>
                <div className="card-body">
                    <h5 className="card-title">Special title treatment</h5>
                    <p className="card-text" style={ this.setStyle() }>With supporting text below as a natural lead-in to additional content.</p>
                    <a className="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        );
    }
}

export default Result;
