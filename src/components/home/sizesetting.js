import React, { Component } from 'react';

class SizeSetting extends Component {
    changeSizeDown(){
        this.props.onChangeSizeDown()
    }
    changeSizeUp(){
        this.props.onChangeSizeUp()
    }
    render() {
        return (
            <div className="col">
                <div className="card" >
                    <div className="card-header">Size: {this.props.fontSize}</div>
                    <div className="card-body">
                        <button className="btn btn-danger" onClick={() => this.changeSizeDown()}>Down</button>
                        <button className="btn btn-success" onClick={() => this.changeSizeUp()}>Up</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default SizeSetting;
